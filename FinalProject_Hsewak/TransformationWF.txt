# @begin ColumnTransformation @desc Display one or more greetings to the user.
# @param MarketName
# @param Website
# @param Facebook
# @param Twitter
# @param Youtube
# @param OtherMedia
# @param Street
# @param City
# @param County
# @param State
# @param Organic

# @out OpenRefine_Cleaned_results @desc Greeting displayed to user.
    # @begin Trim_Value @desc Delete leading and trailing spaces
    # @param MarketName
    # @param Website
    # @param Facebook
    # @param Twitter
    # @param Youtube
    # @param OtherMedia
    # @param Street
    # @param City
    # @param County
    # @param State
    # @param Organic    
    # @out produce_numeric @as result_1
    # @in first_step @as result_1
    # @out first_step @as result_2
    # @end Trim_Value
    
    # @begin FixURL @desc Correct URL typo
    # @param Website
    # @param Facebook
    # @param Twitter
    # @param Youtube
    # @param OtherMedia
    # @in second_step @as result_2
    # @out second_step @as result_3
    # @end Correction
    
    # @begin Cluster_Merge @desc Method: Key collision
    # @in third_step @as result_3
    # @out third_step @as result_4
    # @end Cluster_Merge
    
    # @begin NullFormat @desc replace none, no, n/a, '-' with blank 
    # @in last_step @as result_4
    # @out last_step @as OpenRefineCleaned @file stream:stdout
    # @end NullFormat
   

# @end ColumnTransformation